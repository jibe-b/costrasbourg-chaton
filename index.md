---
title: costrasbourg chaton
---

Bienvenue sur la page du réseau CoStrasbourg !

## Pourquoi

- Que l'on soit un citoyen·ne·s et organisations nous avons tous des ressources (compétence, matériel, connaissance, ...) qui peuvent être utile à d'autres.
- Globalement, nos besoins respectifs sont assez similaires : trouver un lieu où se réunir, communiquer, accueillir de nouveaux bénévoles, ...
- L'entraide inter-collectif permet de faire grandir nos projets tous ensemble
- Notre engagement nous empêche de passer du temps à coopérer #NezDansLeGuidon

## Qui sommes-nous ?

L'idée de la démarche CoStrasbourg est née dans les têtes Tom de Communecter et Jibé (sans-bannière), mais nous sommes nombreu⋅x⋅ses à nous intérresser à la mise en réseau (vous en faites sûrement partie).

Notre ambition est que cette coopération soit portée collectivement. Dès que la dynamique sera appropriée par les organisations de Strasbourg et qu'elle s'auto-alimentera grâce à la contribution de chacun notre objectif sera atteint ! Le but ultime étant que les alternatives écologiques et solidaires deviennent la norme.

Comptez sur nous pour vous fournir exclusivement des logiciels libres, des données ouvertes ;). Niveau gouvernance, on propose systématiquement des modes d'auto-organisation. Nous préférons le modèle de fédération et nous refusons la création de nouveaux niveaux de hiérarchie.

(aidez-nous à quitter facebook pleaaaaaase !)