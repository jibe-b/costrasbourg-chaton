---
title: costrasbourg chaton
---

Bienvenue sur la page du réseau CoStrasbourg !

## Pourquoi

On a tous des ressources à partager (citoyen·ne·s et organisations) : des compétences, du matériel, etc.

Nos besoins sont similaires sur le territoire.

Tout le mond à gagne à mettre en partage.

Nos actions au sein des organisations nous empêche de passer du temps à la coopération #LeNezDansLeGuidon

## Qui sommes-nous

L'idée de mise en réseau CoStrasbourg est née dans les têtes de plusieurs personnes
et nous sommes deux gus, Tom de Communecter et Jibé (sans-bannière) à booster la dynamique
pour le moment.

Dès que la dynamique aura été appropriée par les organisations de Strasbourg, notre objectif
sera atteint !

D'ici là, on est motivés par les questions d'écologie, de solidarité avec le but
que les alternatives deviennent la norme.

Comptez sur nous pour vous fournir exclusivement des logiciels libres, des données ouvertes,

et au niveau gouvernance, on propose systématiquement des modes d'auto-gestion en proposant
un niveau de fédération (et non pas un niveau de hiérarchie supplémentaire).

(aidez-nous à quitter facebook pleaaaaaase !)
