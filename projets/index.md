---
title: Projets du réseau CoStrasbourg
---

## Événementiel

- Évènementiel
    - [Les assos invitent les assos](https://docs.google.com/document/d/1g-aE5LlOkVp8-9mQ_cuXyYuh1bNcQhIEbPF3teUXzlk/edit?usp=sharing)
    - [Première semaine contributive](#première-semaine-contributive)
- Mutualisation d'information
    - Via [Communecter](http://communecter.org/) (réseau social éthique)
        - [Carte des initiatives locales](https://news.defricheurs.fr/carte/)
        - [Lieux acceptant les affiches d'évènements](https://communecter.org/#search?text=#AccepteAffiche)
        - [Lieux zéro déchet](https://zds.fr/adressesfaq/carte-zero-dechet/)
    - Via Facebook
        - [Foodsharing](https://www.facebook.com/events/2386071321425800/)
        - [Évènements locaux par thématique](https://www.facebook.com/permalink.php?story_fbid=2055133834535175&id=2055111037870788)
    - [Listes de salles de réunion](https://lite.framacalc.org/0tn7ulurpm-salles-associations-strasbourg)
    - [Inventaire de matériel](https://lite.framacalc.org/99hqafzozd-materiel-mutualises-collectifs-stras)
    - [Inventaire de machines](https://lite.framacalc.org/l57jkvb7kp-mutualisation-machines-fixes-strasbourg)
- Échanges et entraide
    - [Liste de discussions mail](https://framalistes.org/sympa/info/costrasbourg)
    - [Groupe costrasbourg dans communecter](https://communecter.org/#@costrasbourg)
    - [Échanges de livres](https://inventaire.io/groups/lectures-climatiques-strasbourg)
    - [Rôle interface pour les demandes des autres collectifs](https://hackmd.co.tools/KYZgbATArAjFDsBaAHAYxgQ0QFgEYDNlFcIBOUxYfMTCABhABN5cwg==#)
- Communication
    - [Bonnes pratiques](https://hackmd.co.tools/s/Sk7NGQ2wN#)



